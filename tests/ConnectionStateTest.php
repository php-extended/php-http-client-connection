<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-connection library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpClient\ConnectionState;
use PHPUnit\Framework\TestCase;

/**
 * ConnectionStateTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\HttpClient\ConnectionState
 *
 * @internal
 *
 * @small
 */
class ConnectionStateTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ConnectionState
	 */
	protected ConnectionState $_object;

	public function testGetHeaderValue() : void
	{
		$this->assertSame('close', $this->_object->getHeaderValue());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = ConnectionState::CLOSE;
	}
	
}
