<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-connection library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

/**
 * ConnectionState class file.
 * 
 * This class represents all the possible states that the connection header
 * can take.
 * 
 * @author Anastaszor
 */
enum ConnectionState : string
{
	
	/**
	 * Gets the header value of this state.
	 * 
	 * @return string
	 */
	public function getHeaderValue() : string
	{
		return $this->value;
	}
	case CLOSE = 'close';
	case KEEP_ALIVE = 'keep-alive';
	
}
