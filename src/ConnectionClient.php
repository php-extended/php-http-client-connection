<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-connection library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use InvalidArgumentException;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Stringable;

/**
 * ConnectionClient class file.
 * 
 * This class is an implementation of a client which adds connection headers on
 * incoming requests.
 * 
 * @author Anastaszor
 */
class ConnectionClient implements ClientInterface, Stringable
{
	
	/**
	 * The inner client.
	 * 
	 * @var ClientInterface
	 */
	protected ClientInterface $_client;
	
	/**
	 * The configuration.
	 * 
	 * @var ConnectionConfiguration
	 */
	protected ConnectionConfiguration $_configuration;
	
	/**
	 * Builds a new DoNotTrackClient with the given inner client.
	 * 
	 * @param ClientInterface $client
	 * @param ConnectionConfiguration $configuration,
	 */
	public function __construct(ClientInterface $client, ?ConnectionConfiguration $configuration = null)
	{
		$this->_client = $client;
		if(null === $configuration)
		{
			$configuration = new ConnectionConfiguration();
		}
		$this->_configuration = $configuration;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Client\ClientInterface::sendRequest()
	 */
	public function sendRequest(RequestInterface $request) : ResponseInterface
	{
		if(!$request->hasHeader('Connection'))
		{
			try
			{
				$request = $request->withHeader('Connection', $this->_configuration->getState()->getHeaderValue());
			}
			catch(InvalidArgumentException $e)
			{
				// nothing to do
			}
		}
		
		return $this->_client->sendRequest($request);
	}
	
}
