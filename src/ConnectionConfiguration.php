<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-connection library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use Stringable;

/**
 * ConnectionConfiguration class file.
 * 
 * This class stores all the configuration options for the connection client.
 * 
 * @author Anastaszor
 */
class ConnectionConfiguration implements Stringable
{
	
	/**
	 * The state of the connection.
	 * 
	 * @var ?ConnectionState
	 */
	protected ?ConnectionState $_state = null;
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the state of the connection.
	 * 
	 * @param ConnectionState $state
	 */
	public function setState(ConnectionState $state) : void
	{
		$this->_state = $state;
	}
	
	/**
	 * Gets the state of the connection.
	 * 
	 * @return ConnectionState
	 * @SuppressWarnings("PHPMD.StaticAccess")
	 */
	public function getState() : ConnectionState
	{
		if(null === $this->_state)
		{
			$this->_state = ConnectionState::CLOSE;
		}
		
		return $this->_state;
	}
	
}
